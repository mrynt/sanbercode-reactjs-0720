//soal 1
var mulai = 2;
var limit = 2;
while(mulai <= 20) { 
   
   
    if(mulai == 2){
        console.log('LOOPING PERTAMA');
    }
   
    console.log(mulai+ ' - I love coding'); 
    mulai += limit;
    if(mulai > 20){
        console.log('LOOPING KEDUA');
        loop2 = mulai - limit;
        while(loop2 >= limit) { 
            console.log(loop2+ ' - I will become a frontend developer'); 
            loop2 -= limit;
        }
    }
}


//soal 2
for(i=1;i<=20;i++){
    if(i%2 == 0){
        console.log(i+ ' - Berkualitas'); 
    }else if(i%3 == 0){
        console.log(i+ ' - I Love Coding'); 
    }else{
        console.log(i+ ' - Santai'); 
    }
}

//soal 3
for(x = 1; x <= 7; x++){
    string = '';
    for(z=1;z <= x;z++){
        string += '#';
        
    }
    console.log(string);
    
}

//soal 4
var kalimat="saya sangat senang belajar javascript"
var ar = kalimat.split(' ');
console.log(ar);

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var x = daftarBuah.sort().join("\n");
console.log(x);