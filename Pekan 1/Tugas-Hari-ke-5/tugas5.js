//soal 1

function halo(){
    return 'Halo Sanbers!';
}

console.log(halo());

function kalikan(par1,par2){
    x = par1 * par2;
    return x;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

//soal 3
/* 
    Tulis kode function di sini
*/
 
function introduce(name,age,address,hobby){
    x = "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!" ;
    return x;
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 