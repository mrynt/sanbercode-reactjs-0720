//soal 1

let  luaslingkaran = (r) => {   
    let phi = 3.14;
	return phi * r *r;
};

console.log(luaslingkaran(10));

let  kelilinglingkaran = (r) => {   
    let phi = 3.14;
    let d = 2 * r;;
	return  phi * d;
};

console.log(kelilinglingkaran(10));


//soal 2
const kata1 = 'saya';
const kata2 = 'adalah';
const kata3 = 'seorang';
const kata4 = 'frontend';
const kata5 = 'developer';
let kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
console.log(kalimat);


//soal 3
 class book{
    constructor(isColorful) {
        this.name = 'buku';
        this.totalPage = '30';
        this.price = 30000;
        this.isColorful = isColorful;
      }
 }


 class komik extends book{
    constructor(isColorful) {
        super(isColorful);
      }
 }

 var komiks = new komik(false)
 console.log(komiks.isColorful)



 //perbaikan
 // soal 2
let kalimat='';
 const tambahankanKata = (str) => {
     kalimat = `${kalimat} ${str}`
 }

 tambahankanKata("saya");
 tambahankanKata("adalah");
 tambahankanKata("seorang");
 tambahankanKata("frontend");
 tambahankanKata("developer");

 console.log(kalimat);


 //soal 3
 class book{
     constructor(name,totalPage,price){
         this.name = name ;
         this.totalPage = totalPage;
         this.price = price;
     }
 }

 class komik extends book{
     constructor(name,totalPage,price,isPowerfull){
         super(name,totalPage,price)
         this.isPowerfull = isPowerfull;
     }
 }

 let novel = new komik("novel",402,50000)