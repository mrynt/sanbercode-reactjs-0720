console.log('------------- soal 1 ---------')

const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 


  console.log('------------- soal 2 ---------')
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const {firstName , lastName,destination,occupation,spell} = newObject;

console.log(firstName,lastName)


console.log('------------- soal 3 ---------')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code

const combinedes6 = [...west, ...east]
console.log(combinedes6)