import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="flexbox-container">
      <div className="flexbox-item fixed">
        <form>
          <div className="w-100 aligncenter mb-20">
            <h2><b>Form Pemesanan Buah</b></h2>
          </div>
          <div className="w-100 mb-20">
              <label className="col-3 bold" htmlFor="textinput">Nama Pelanggan</label>
              <div className="col-7">
                  <input id="textinput" className="w-100" name="nama_pelanggan" type="text" placeholder="Nama Pelanggan" />
              </div>
          </div>
          <div className="w-100">
            <label className="col-3 bold" htmlFor="textinput">Daftar Item</label>
            <div className="col-7">
                <div className="checkbox-w">
                  <input type="checkbox" id="Semangka"/>
                  <label htmlFor="customCheck1"> Semangka</label>
								</div>
                <div className="checkbox-w">
                  <input type="checkbox" id="Jeruk"/>
                  <label htmlFor="customCheck1"> Jeruk</label>
								</div>
                <div className="checkbox-w">
                  <input type="checkbox" id="Nanas"/>
                  <label htmlFor="customCheck1"> Nanas</label>
								</div>
                <div className="checkbox-w">
                  <input type="checkbox" id="Salak"/>
                  <label htmlFor="customCheck1"> Salak</label>
								</div>
                <div className="checkbox-w">
                  <input type="checkbox" id="Anggur"/>
                  <label htmlFor="customCheck1"> Anggur</label>
								</div>
            </div>
          </div>
          <div className="w-100">
					<button  id="button1id" name="button1id" className="btn">Kirim</button>
				  </div>
        </form>

      </div>
    </div>
  );
}

export default App;
