//soal 1

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var arrobj = {nama: arrayDaftarPeserta[0],
              jeniskelamin: arrayDaftarPeserta[1],
              hobi: arrayDaftarPeserta[2],
              tahunlahir: arrayDaftarPeserta[3]}
console.log(arrobj);


//soal 2
arrobj2 = [
    {nama: 'strawberry',warna:'merah',ada_bijinya:'tidak',harga:9000},
    {nama: 'jeruk',warna:'oranye',ada_bijinya:'ada',harga:8000},
    {nama: 'Semangka',warna:'Hijau & Merah',ada_bijinya:'ada',harga:10000},
    {nama: 'Pisang',warna:'Kuning',ada_bijinya:'tidak',harga:5000},
];

console.log(arrobj2[0]);


//soal 3
var dataFilm = []
function addfilm(arr){
    data = {
        nama:arr[0],
        durasi:arr[1],
        genre:arr[2],
        tahun:arr[3],
    }

    dataFilm.push(data);

}

addfilm(['film 1','1 jam','Horor','2020']);
console.log(dataFilm);

//soal 4
class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
      }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(name,cold_blooded) {
      super(name,cold_blooded);
    }

     yell() {
        console.log("Auooo") ;
    }
  }

  class Frog extends Animal {
    constructor(name,legs,cold_blooded) {
      super(name,legs,cold_blooded);
    }

    jump() {
        console.log("hop hop") ;
    }
  }


var sungokong = new Ape("kera sakti")
console.log(sungokong.name)// "Auooo"
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.name)// "Auooo"
kodok.jump() // "hop hop" 


//soal 5
class Clock {
    constructor({template}) {
        this.template = template;
        this.timer;
      }

       render() {
        var date = new Date();
        var template = this.template;
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
      }  

      start()  {
        this.timer = setInterval(() => this.render(), 1000);
      };

      stop(){
        var timer = this.timer;
        clearInterval(timer);
      }

     
      
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
//clock.stop();  

